var express = require('express');
var router = express.Router();

const AuthService = require('./../../services/AuthService');
const ArticleController = require('./../../controller/article_controller/ArticleSubmission')

// GET DATA
router.get('/', ArticleController.getArticlePostList);
router.get('/metadata', ArticleController.getMetadata);
router.get('/:id', ArticleController.getArticlePostById);

// Inserting or Updating article need specific permission
router.use(AuthService.hasAccess(['MASTER-CRUD'])); // temporary disabled

// MANIPULATE DATA
router.post('/', ArticleController.submitArticlePost);

module.exports = router;