var express = require('express');
var router = express.Router();

const AuthService = require('./../../services/AuthService');



//PLANT APP

// const DepartmentController = require('../../controller/plant_action_master_controller/DepartmentController');
// const MeetingManagementController = require('../../controller/plant_action_master_controller/MeetingManagementController');

// GET DATA

const DepartmentController = require('../../controller/master_data_controller/DepartmentController');


//department data
router.get('/department', DepartmentController.getAllDepartment)
router.put('/department/:id', DepartmentController.updateDepartment)
router.post('/department', DepartmentController.insertDepartment);

//meeting management data
// router.get('/monthly-meeting', MeetingManagementController.getAllMeeting)


// Inserting or Updating master table need specific permission
// router.use(AuthService.hasAccess(['MASTER-CRUD']));

// temporary disabled

// MANIPULATE DATA

// department data
router.post('/department', DepartmentController.insertDepartment);
// router.put('/department/:id', DepartmentController.updateDepartment);
// meeting data
// router.post('/monthly-meeting', MeetingManagementController.insertMeeting);
// router.put('/monthly-meeting/:id', MeetingManagementController.updateMeeting);



module.exports = router;