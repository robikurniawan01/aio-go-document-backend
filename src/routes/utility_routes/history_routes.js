var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');

const HistoryController = require("./../../controller/utility_controller/HistoryController");

// User data
router.get('/log-data', HistoryController.getAllLog);
router.get('/log-data/:table/:id', HistoryController.getHistoryByTable);
router.post('/log-data-filter', HistoryController.getFilterLogByDate);

module.exports = router;