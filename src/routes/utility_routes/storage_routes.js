var express = require('express');
var router = express.Router();
var StorageService = require('./../../services/StorageService')


// storage get file
router.get('/download/:filename', StorageService.download);


// Handle file upload and return the filename to the client in real-time
router.post('/upload', StorageService.upload.single('file'), (req, res) => {
  if (!req.file || req.fileValidationError) {
    return res.status(400).json({ error: 'No file uploaded.' });
  }

  // Send the filename back to the client
  const filename = req.file.filename;
  res.status(200).json({ message: 'File uploaded successfully.', filename });
});
  
module.exports = router;