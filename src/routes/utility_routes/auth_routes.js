var express = require('express');
var router = express.Router();


const AuthController = require("./../../controller/auth_controller/AuthController");
// const UserController = require("./../../controller/user_controller/UserController");
// const NotificationController = require("./../../controller/utility_controller/NotificationController");

router.post('/login', AuthController.logIn);
router.post('/register', AuthController.register);
router.post('/check-email-reset', AuthController.checkEmailReset);
router.put('/reset-password/:id', AuthController.resetPassword);

// User data
// router.post('/user', UserController.insertUser);
// router.put('/user/:id', UserController.updateUser);

// // Role
// router.get('/role', UserController.getAllRole);
// router.post('/role', UserController.insertRole);
// router.put('/role/:id', UserController.updateRole);

// // Permission
// router.get('/permission', UserController.getAllPermission);
// router.get('/permission/:id', UserController.getPermissionByRoleId);


module.exports = router;