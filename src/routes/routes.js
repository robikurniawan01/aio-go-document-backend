var express = require('express');
var router = express.Router();
var path = require('path');

// master data routes file
const master_data_routes = require('./master_routes/master_routes_data');
// const article_data_routes = require('./master_routes/article_routes_data');
const auth_routes = require('./utility_routes/auth_routes');

// micro site routes file 
// const micro_site_routes = require('./micro_site_routes/micro_site_routes');
// transaction routes file


// utility routes file
const storage_routes = require('./utility_routes/storage_routes');


// auth service
// const AuthService = require('./../services/AuthService');
const LogService = require('./../services/LogService');

// transaction
// const articleRoutes = require('./transaction_routes/article_routes');
// const commentRoutes = require('./transaction_routes/comment_article_routes');
// const tasklistRoutes = require('./transaction_routes/tasklist_routes');
// plant route aio employee
const plantListMasterEmployeeRoute = require('./plant_routes/master_aio_employe_routes/master_aio_employee');
// plant route departement
const plantListMasterDepartementRoute = require('./plant_routes/master_aio_employe_routes/master_departement');
// plant route role
const plantListMasterRoleRoute = require('./plant_routes/master_aio_employe_routes/master_role');
// plant route auth
const plantListMasterAuthRoute = require('./plant_routes/master_aio_employe_routes/master_authorization');
// plant route attedance
const plantListMasterAttendanceRoute = require('./plant_routes/master_aio_employe_routes/master_attedance');

// home api endpoint
router.get('/', (req, res) => res.status(200).json({ 'Service': "Go Document API" }));


// start logging function
router.use(LogService.logMiddleware);

// authentication routes usage 
router.use('/auth/', auth_routes);

///////////////////// starting endpoint no needs authentication /////////////////////
// router.use('/micro-site', micro_site_routes);
// router.use('/articles', articleRoutes);
// router.use('/comments', commentRoutes);
// router.use('/task-list', tasklistRoutes);



///////////////////// starting endpoint needs authentication /////////////////////
// router.use(AuthService.authenticateToken);

// master data routes usage 
router.use('/master/', master_data_routes);
// router.use('/article-post/', article_data_routes);

// utility
router.use('/storage/', storage_routes)

// not found route
router.get('/not-found', function (req, res) {
  res.status(404).sendFile(path.join(__dirname, '../views/not-found.html'));
});


module.exports = router;