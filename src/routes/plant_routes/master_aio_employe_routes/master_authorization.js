const express = require('express');
const router = express.Router();
const authController = require('../../../controller/plant_controllers/master_authorization_controller/master_authorization_controller');

router.get('/getAllAuth', authController.allAuth);
router.post('/getAllAuthorizationJoinPaginate', authController.getAllAuthWithDepartement);
router.delete('/deleteAuthById/:id', authController.deleteAuthById);
router.post('/createAuth', authController.createAuth);
router.put('/updateAuth/:id', authController.updateAuth);

module.exports = router;
