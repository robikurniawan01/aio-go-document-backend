const express = require('express');
const router = express.Router();
const departementController = require('../../../controller/master_data_controller/DepartmentController');

router.get('/getAllDepartement', departementController.getAllDepartment);

module.exports = router;
