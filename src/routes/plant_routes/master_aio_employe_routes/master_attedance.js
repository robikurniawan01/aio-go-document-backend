const express = require('express');
const router = express.Router();
const attedanceController = require('../../../controller/plant_controllers/master_attedance_controller/master_attedance_controller');

router.post('/getAllAttendance', attedanceController.getAllAttendance);
router.post('/upSetAttedance', attedanceController.upsetAttendance);
module.exports = router;
