const express = require('express');
const router = express.Router();
const employeeController = require('../../../controller/plant_controllers/master_aio_employee_controler/master_aio_employee_controller');

router.post('/searchEmployee', employeeController.searchEmployee);
router.get('/allEmployee', employeeController.allEmployee);

module.exports = router;
