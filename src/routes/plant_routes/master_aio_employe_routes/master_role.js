const express = require('express');
const router = express.Router();
const roleController = require('../../../controller/plant_controllers/master_role_controller/master_role_controller');

router.get('/getAllRole', roleController.allRole);

module.exports = router;
