const express = require('express');
const router = express.Router();
const tasklistController = require('../../controller/transaction_controllers/TasklistController');

router.get('/', tasklistController.getAll);
router.post('/get-where', tasklistController.getWhere);

module.exports = router;