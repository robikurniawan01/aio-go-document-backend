const express = require('express');
const router = express.Router();
const articleController = require('../../controller/transaction_controllers/ArticleController');

router.get('/', articleController.getComments);
router.post('/', articleController.postComment);

module.exports = router;