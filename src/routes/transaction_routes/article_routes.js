const express = require('express');
const router = express.Router();
const articleController = require('../../controller/transaction_controllers/ArticleController');

router.get('/', articleController.getAllArticle);
router.get('/:slug', articleController.getArticle);

module.exports = router;