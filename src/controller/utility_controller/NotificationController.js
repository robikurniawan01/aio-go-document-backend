const api = require("../../tools/common");
const NotificationService = require('../../services/NotificationService');

sendNotification = async (req, res) => {
    const { type, param, config } = req.body
    const result = await NotificationService.sendNotification(type, param);
    return api.ok(res, result);
}

module.exports = api.handleError({
    sendNotification
});