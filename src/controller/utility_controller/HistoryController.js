const api = require("../../tools/common");
const model = require("../../model/user_data/history_model");
const moment = require ('moment')

getAllLog = async (req, res) => {
    let data = await model.getAll('v_logs');
    return api.ok(res, data);
}

getFilterLogByDate = async (req, res) => {
    if (req.body.limit == 0) {
        let data = await model.getWhereDateRange('v_logs', 'created_at', req.body.start, req.body.end);
        return api.ok(res, data);
    } else {
        let data = await model.getWhereDateRangeWithLimit('v_logs', 'created_at', req.body.start, req.body.end, req.body.limit);
        return api.ok(res, data);
    }

}

getHistoryByTable = async (req, res) => {
    let data = await model.getWhereAnd('v_logs', 'table', req.params.table, 'param', req.params.id);
    return api.ok(res, data);
}

module.exports = api.handleError({
    // Log
    getAllLog,
    getFilterLogByDate,
    getHistoryByTable
});