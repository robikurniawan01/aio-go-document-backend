const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllPermission = async (req, res) => {
    let data = await model.getAll('mst_permission');
    return data.length > 0 ? api.ok(res, data) : api.error(res, "not found", 404);
}

module.exports = api.handleError({
    getAllPermission
});