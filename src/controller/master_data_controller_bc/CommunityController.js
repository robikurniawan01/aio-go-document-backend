const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllCommunity = async (req, res) => {
    let data = await model.getAll('mst_community');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getCommunityById = async (req, res) => {
    let data = await model.getById('mst_community', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertCommunity = async (req, res) => {
    let data = await model.insert('mst_community', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateCommunity = async (req, res) => {
    let data = await model.update('mst_community', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllCommunity,
    getCommunityById,
    insertCommunity,
    updateCommunity
});