const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllProduct = async (req, res) => {
    let data = await model.getAll('mst_product');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getProductById = async (req, res) => {
    let data = await model.getById('mst_product', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertProduct = async (req, res) => {
    let data = await model.insert('mst_product', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateProduct = async (req, res) => {
    let data = await model.update('mst_product', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllProduct,
    getProductById,
    insertProduct,
    updateProduct
});