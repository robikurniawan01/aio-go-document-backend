const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllCategory = async (req, res) => {
    let data = await model.getAll('mst_article_category');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getCategoryById = async (req, res) => {
    let data = await model.getById('mst_article_category', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getCategoryWhere = async (req, res) => {
    let data = await model.getWhere('mst_article_category', [], 1000);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertCategory = async (req, res) => {
    let data = await model.insert('mst_article_category', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateCategory = async (req, res) => {
    let data = await model.update('mst_article_category', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllCategory,
    getCategoryById,
    getCategoryWhere,
    insertCategory,
    updateCategory
});