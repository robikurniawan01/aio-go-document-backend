const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllArticleComment = async (req, res) => {
    let data = await model.getAll('tr_article_comment');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getArticleCommentById = async (req, res) => {
    let data = await model.getById('tr_article_comment', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertArticleComment = async (req, res) => {
    let data = await model.insert('tr_article_comment', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateArticleComment = async (req, res) => {
    let data = await model.update('tr_article_comment', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllArticleComment,
    getArticleCommentById,
    insertArticleComment,
    updateArticleComment
});