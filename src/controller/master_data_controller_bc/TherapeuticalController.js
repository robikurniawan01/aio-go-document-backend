const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");


getAllTherapeutical = async (req, res) => {
    let data = await model.getAll('mst_therapeutical');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getTherapeuticalById = async (req, res) => {
    let data = await model.getById('mst_therapeutical', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertTherapeutical = async (req, res) => {
    let data = await model.insert('mst_therapeutical', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateTherapeutical = async (req, res) => {
    let data = await model.update('mst_therapeutical', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}



module.exports = api.handleError({
    getAllTherapeutical,
    getTherapeuticalById,
    insertTherapeutical,
    updateTherapeutical,
});