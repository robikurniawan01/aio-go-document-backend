const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllRole = async (req, res) => {
    let data = await model.getAll('v_role_permission');
    return data.length > 0 ? api.ok(res, data) : api.error(res, "not found", 404);
}

insertRole = async (req, res) => {
    try {
        const { selectedPermission } = req.body.form_data;
        delete req.body.form_data.selectedPermission;
        let role = await model.insert('mst_role', { ...req.body.form_data, created_by: req.user.id });
        for (permission of selectedPermission) {
            await model.insert('map_role_permission', {
                role_id: role[0],
                permission_id: permission,
                created_by: req.user.id
            });
        }
        let data = {
            permission
        }
        return api.ok(res, data);
    } catch (err) {
        return api.error(res, `Error Argument! ${err}`, 403);
    }
}

updateRole = async (req, res) => {
    if (req.body.form_data.status == 'deleted') {
        let data = await model.update('mst_role', req.params.id, { ...req.body.form_data, updated_by: req.user.id });
        return api.ok(res, data);
    } else {
        const { selectedPermission } = req.body.form_data;
        delete req.body.form_data.selectedPermission;
        let roleData = await model.update('mst_role', req.params.id, { ...req.body.form_data, updated_by: req.user.id });
        let deletedData = await model.deleteDataWhere('map_role_permission', 'role_id', req.params.id)
        try {
            for (permission of selectedPermission) {
                await model.insert('map_role_permission', {
                    role_id: req.params.id,
                    permission_id: permission,
                    created_by: req.user.id
                });
            }
            return api.ok(res, [roleData, deletedData, permission]);
        } catch (err) {
            return api.error(res, `Error Argument! ${err}`, 403);
        }
    }
}

module.exports = api.handleError({
    getAllRole,
    insertRole,
    updateRole
});