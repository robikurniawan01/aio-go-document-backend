const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllArticleMedia = async (req, res) => {
    let data = await model.getAll('tr_article_media');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getArticleMediaById = async (req, res) => {
    let data = await model.getById('tr_article_media', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertArticleMedia = async (req, res) => {
    let data = await model.insert('tr_article_media', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateArticleMedia = async (req, res) => {
    let data = await model.update('tr_article_media', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}


module.exports = api.handleError({
    getAllArticleMedia,
    getArticleMediaById,
    insertArticleMedia,
    updateArticleMedia
});