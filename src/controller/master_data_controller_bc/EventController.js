const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllEvent = async (req, res) => {
    let data = await model.getAll('mst_event');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getEventById = async (req, res) => {
    let data = await model.getById('mst_event', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertEvent = async (req, res) => {
    let data = await model.insert('mst_event', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateEvent = async (req, res) => {
    let data = await model.update('mst_event', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllEvent,
    getEventById,
    insertEvent,
    updateEvent
});