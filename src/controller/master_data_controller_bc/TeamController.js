const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllTeam = async (req, res) => {
    let data = await model.getAll('mst_team');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getTeamById = async (req, res) => {
    let data = await model.getById('mst_team', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertTeam = async (req, res) => {
    let data = await model.insert('mst_team', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateTeam = async (req, res) => {
    let data = await model.update('mst_team', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllTeam,
    getTeamById,
    insertTeam,
    updateTeam
});