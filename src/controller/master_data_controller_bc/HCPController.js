const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllHcp = async (req, res) => {
    let data = await model.getAll('mst_hcp');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getHcpById = async (req, res) => {
    let data = await model.getById('mst_hcp', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertHcp = async (req, res) => {
    let data = await model.insert('mst_hcp', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateHcp = async (req, res) => {
    let data = await model.update('mst_hcp', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllHcp,
    getHcpById,
    insertHcp,
    updateHcp
});