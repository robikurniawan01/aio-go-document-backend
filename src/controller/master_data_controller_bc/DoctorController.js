const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllDoctor = async (req, res) => {
    let data = await model.getAll('v_doctor');
    return data.length > 0 ? api.ok(res, data) : api.error(res, "not found", 404);
}

insertDoctor = async (req, res) => {
    try {
        const { selectedTherapeuticals } = req.body.form_data;
        delete req.body.form_data.selectedTherapeuticals;
        let doctor = await model.insert('tr_doctor', { ...req.body.form_data, created_by: req.user.id });
        for (therapeutical of selectedTherapeuticals) {
            await model.insert('map_doctor_therapeutical', {
                doctor_id: doctor[0],
                therapeutical_id: therapeutical,
                created_by: req.user.id
            });
        }
        let data = {
            therapeutical
        }
        return api.ok(res, data);
    } catch (err) {
        return api.error(res, `Error Argument! ${err}`, 403);
    }
}

updateDoctor = async (req, res) => {
    if (req.body.form_data.status == 'deleted') {
        let data = await model.update('tr_doctor', req.params.id, { ...req.body.form_data, updated_by: req.user.id });
        return api.ok(res, data);
    } else {
        const { selectedTherapeuticals } = req.body.form_data;
        delete req.body.form_data.selectedTherapeuticals;
        let doctorData = await model.update('tr_doctor', req.params.id, { ...req.body.form_data, updated_by: req.user.id });
        let deletedData = await model.deleteDataWhere('map_doctor_therapeutical', 'doctor_id', req.params.id)
        try {
            for (therapeutical of selectedTherapeuticals) {
                await model.insert('map_doctor_therapeutical', {
                    doctor_id: req.params.id,
                    therapeutical_id: therapeutical,
                    created_by: req.user.id
                });
            }
            return api.ok(res, [doctorData, deletedData, therapeutical]);
        } catch (err) {
            return api.error(res, `Error Argument! ${err}`, 403);
        }
    }
}

module.exports = api.handleError({
    getAllDoctor,
    insertDoctor,
    updateDoctor
});