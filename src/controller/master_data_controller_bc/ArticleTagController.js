const model = require("../../model/master_data/master_data_model");
const api = require("../../tools/common");

getAllArticleTag = async (req, res) => {
    let data = await model.getAll('mst_tag');
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getArticleTagById = async (req, res) => {
    let data = await model.getById('mst_tag', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getArticleTagWhere = async (req, res) => {
    let data = await model.getWhere('mst_tag', [], 1000);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertArticleTag = async (req, res) => {
    let data = await model.insert('mst_tag', { ...req.body.form_data, created_by: req.user.id  });
    return api.ok(res, data);
}

updateArticleTag = async (req, res) => {
    let data = await model.update('mst_tag', req.params.id, { ...req.body.form_data, updated_by: req.user.id  });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllArticleTag,
    getArticleTagById,
    getArticleTagWhere,
    insertArticleTag,
    updateArticleTag
});