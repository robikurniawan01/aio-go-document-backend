const model = require("../../model/master_data/master_data_model");
const articleModel = require("../../model/article/article");
const api = require("../../tools/common");
const moment = require("moment")

const ArticleSubmissionController = {
    getArticlePostList: async (req, res) => {
        const countPost = await articleModel.countPostStatus();
        const countTotalPost = await articleModel.countWhere();
        const countPostData = {};
        countPost.forEach((row) => {
            countPostData[row.post_status] = row.count;
        });
        let conditions = [];
        if(req.user.permissions.includes('MASTER-CRUD')){
            conditions = [];
        } else {
            [
                {field: 'created_by', operator: '=', value: req.user.id}
            ]
        }
        const result =  {
            posts: await model.getWhere('v_article', conditions, req.query.page, req.query.size),
            postCount: countPostData,
            totalPosts: countTotalPost[0].count
        };
        return result.posts.length > 0? api.ok(res, result) : api.error(res, "not found", 404);
    },
    getMetadata: async (req, res) => {
        let data = {
            therapeuticals: await model.getWhere('mst_therapeutical', [], 1, 1000),
            categories: await model.getWhere('mst_article_category', [], 1, 1000),
            tags: await model.getWhere('mst_tag', [], 1, 1000),
        } 
        return api.ok(res, data);
    },
    getArticlePostById: async (req, res) => {
        let data = await model.getWhere('tr_article', [{field: 'id', operation: '=', value: req.params.id}]); 
        return data.length > 0? api.ok(res, data[0]) : api.error(res, "not found", 404);
    },
    submitArticlePost: async (req, res) => {
        // console.log(req.body)
        
        try {
            console.log('test')
            const { selectedCategories, selectedTherapeuticals, file, references } = req.body.form_data;
            delete req.body.form_data.selectedTherapeuticals;
            delete req.body.form_data.selectedCategories;
            delete req.body.form_data.file;
            delete req.body.form_data.references;


            req.body.form_data.tags = req.body.form_data.tags.join(',');
            req.body.form_data.author = req.user.id;
            req.body.form_data.publish_date = moment(req.body.form_data.publish_date).format('YYYY-MM-DD HH:mm:ss');
            let article = await model.insert('tr_article', { ...req.body.form_data, created_by: req.user.id  });
            for(therapeutical of selectedTherapeuticals) {
                await model.insert('map_article_therapeutical', {  
                    article_id: article[0],
                    therapeutical_id: therapeutical, 
                    created_by: req.user.id  
                });
            }

            for(files of file) {
                console.log(files)
                await model.insert('tr_article_journal', {  
                    article_id: article[0],
                    journal_path: files.filename, 
                    created_by: req.user.id  
                });
            }

            for(reference of references) {
                console.log(files)
                await model.insert('tr_article_references', {  
                    article_id: article[0],
                    reference: reference.reference, 
                    created_by: req.user.id  
                });
            }

            for(category of selectedCategories) {
                await model.insert('map_article_category', {  
                    article_id: article[0],
                    category_id: category, 
                    created_by: req.user.id  
                });
            }
            // let dataMeta = await model.insert('tr_article_meta', { ...req.body.form_data, created_by: req.user.id  });
            let dataTasklist = [];
            if(req.body.form_data.post_status != 'Draft'){
                dataTasklist = await model.insert('tr_tasklist', {
                     type: 'article',
                     param_id: article[0],
                     status: 'submitted',
                     created_by: req.user.id  
                    }
                );
            }
            let data = {
                article,
                dataTasklist
            }
            return api.ok(res, data);
        } catch(err) {
            return api.error(res, `Error Argument! ${err}`, 403);
        } 
    },
}

module.exports = api.handleError(ArticleSubmissionController);