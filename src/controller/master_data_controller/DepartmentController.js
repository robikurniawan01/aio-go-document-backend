const model = require("../../model/go_document/master_data.model");
const api = require("../../tools/common");

getAllDepartment = async (req, res) => {
    let data = await model.getAll('mst_department');
    // console.log(data)

    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getDepartmentById = async (req, res) => {
    let data = await model.getById('mst_department', req.params.id);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

getDepartmentWhere = async (req, res) => {
    let data = await model.getWhere('mst_department', [], 1000);
    return data.length > 0? api.ok(res, data) : api.error(res, "not found", 404);
}

insertDepartment = async (req, res) => {
    let data = await model.insert('mst_department', { ...req.body.form_data });
    return api.ok(res, data);
}

updateDepartment = async (req, res) => {
    let data = await model.update('mst_department', req.params.id, { ...req.body.form_data });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllDepartment,
    getDepartmentById,
    getDepartmentWhere,
    insertDepartment,
    updateDepartment
});