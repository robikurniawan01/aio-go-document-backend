const model = require("../../model/user_data/user_data_model");
const api = require("../../tools/common");

// User
getAllUser = async (req, res) => {
    let data = await model.getAll('v_users');
    return api.ok(res, data);
}

insertUser = async (req, res) => {
    let data = await model.insert('mst_user', req.body.form_data);
    return api.ok(res, data);
}

updateUser = async (req, res) => {
    let data = await model.update('mst_user', req.params.id, req.body.form_data);
    return api.ok(res, data);
}

// Role
getAllRole = async (req, res) => {
    let data = await model.getAll('v_roles');
    return api.ok(res, data);
}

insertRole = async (req, res) => {
    const dataLempar = req.body.form_data
    let insertRole = {
        role_name: dataLempar.role_name,
        detail: dataLempar.detail
    }
    let dataHeader = await model.insert('mst_user_role', insertRole);

    for(let i = 0; i < dataLempar.permissions.length; i++){
        let insertPermissions = {
            role_id: dataHeader,
            permission_id: dataLempar.permissions[i],
            created_by: dataLempar.created_by
        }

        let data = await model.insert('map_role_permission', insertPermissions);
        if (i == dataLempar.permissions.length - 1) {
            return api.ok(res, data);
        }
    }
}

updateRole = async (req, res) => {
    // Update Values
    const dataLempar = req.body.form_data
    let updateRole = {
        role_name: dataLempar.role_name,
        detail: dataLempar.detail
    }

    // Update service
    let data = await model.update('mst_user_role', req.params.id, updateRole);

    // Delete Permission Service
    await model.deleteDataPermission('map_role_permission', req.params.id);
    
    // Update New Permission
    for(let i = 0; i < dataLempar.permissions.length; i++){
        let insertPermissions = {
            role_id: req.params.id,
            permission_id: dataLempar.permissions[i],
            created_by: dataLempar.created_by
        }

        let data = await model.insert('map_role_permission', insertPermissions);
        if (i == dataLempar.permissions.length - 1) {
            return api.ok(res, data);
        }
    }
}

// Permission
getAllPermission = async (req, res) => {
    let data = await model.getAll('mst_user_permission');
    return api.ok(res, data);
}

getPermissionByRoleId = async (req, res) => {
    if(!isNaN(req.params.id)){
        let data = await model.getWhere('v_roles', 'id', req.params.id);
        return api.ok(res, data);
    } else {
        return api.error(res, "bad request", 400);
    }
}

module.exports = api.handleError({
    // User
    getAllUser,
    insertUser,
    updateUser,

    // Role
    getAllRole,
    insertRole,
    updateRole,

    // Permission
    getAllPermission,
    getPermissionByRoleId
});