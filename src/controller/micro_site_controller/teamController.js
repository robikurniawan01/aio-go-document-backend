const TeamModel = require('../../model/micro_site/mst_team_model'); // Import model

// Mendapatkan semua data tim
const getAllTeams = async (req, res) => {
  try {
    const teams = await TeamModel.getAllTeams().orderBy('id','desc');;
    res.json(teams);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = {
  getAllTeams,
};
