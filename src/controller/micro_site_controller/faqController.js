const FaqModel = require('../../model/micro_site/mst_faq'); // Import model

// Mendapatkan semua data faq
const getAllFaq = async (req, res) => {
  try {
    const faq = await FaqModel.getAllFaq().orderBy('id','desc');
    res.json(faq);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = {
    getAllFaq,
};
