// Controller
const TrArticle = require('../../model/micro_site/tr_article_model');

const getAllTArticle = async (req, res) => {
    const { page, perPage } = req.body;

    try {
        const pageNumber = parseInt(page) || 1;
        const itemsPerPage = parseInt(perPage) || 4;

        const offset = (pageNumber - 1) * itemsPerPage;
        const totalRecords = await TrArticle.getTotalRecords();
        const articles = await TrArticle.getAllTArticleWithPagination(offset, itemsPerPage);
        const respone = [
            articles,
            totalRecords
        ]
        res.json(respone);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

const getTArticlesByRole = async (req, res) => {
    const { role, page, perPage } = req.body;

    try {
        const pageNumber = parseInt(page) || 1;
        const itemsPerPage = parseInt(perPage) || 4;

        const offset = (pageNumber - 1) * itemsPerPage;

        if (!role) {
            const totalRecords = await TrArticle.getTotalRecords();
            const articles = await TrArticle.getAllTArticleWithPagination(offset, itemsPerPage);
            const respone = [
                articles,
                totalRecords
            ]
            res.json(respone);
        } else {
            const totalRecords = await TrArticle.getTotalRecordsByRole(role);
            const articles = await TrArticle.getTArticlesByRoleWithPagination(role, offset, itemsPerPage);
            const respone = [
                articles,
                totalRecords
            ]
            res.json(respone);
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

module.exports = {
    getAllTArticle,
    getTArticlesByRole,
};
