const CommunityModel = require('../../model/micro_site/mst_community_model'); // Import model

// Mendapatkan semua data community
const getAllCommunity = async (req, res) => {
  try {
    const community = await CommunityModel.getAllCommunity().orderBy('id','desc');
    res.json(community);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = {
    getAllCommunity,
};
