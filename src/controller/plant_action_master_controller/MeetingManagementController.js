const model = require("../../model/plant_action/master_data.model");
const api = require("../../tools/common");

getAllMeeting = async (req, res) => {
    let data = await model.getAll('tr_monthly_meeting');
    console.log(data)
    return data.length > 0 ? api.ok(res, data) : api.error(res, "not found", 404);
}

getDepartmentById = async (req, res) => {
    let data = await model.getById('mst_department', req.params.id);
    return data.length > 0 ? api.ok(res, data) : api.error(res, "not found", 404);
}

getDepartmentWhere = async (req, res) => {
    let data = await model.getWhere('mst_department', [], 1000);
    return data.length > 0 ? api.ok(res, data) : api.error(res, "not found", 404);
}

insertMeeting = async (req, res) => {
    let data = await model.insert('tr_monthly_meeting', { ...req.body.form_data, created_by: req.user.id });
    return api.ok(res, data);
}

updateMeeting = async (req, res) => {
    let data = await model.update('tr_monthly_meeting', req.params.id, { ...req.body.form_data, updated_by: req.user.id });
    return api.ok(res, data);
}

module.exports = api.handleError({
    getAllMeeting,
    getDepartmentById,
    getDepartmentWhere,
    insertMeeting,
    updateMeeting
});