const mstRoleModel = require('../../../model/plant_models/master_role_model');


const allRole = async (req, res) => {
    try {      
      getRole = await mstRoleModel.getAllRole();
  
      res.json(getRole);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };
module.exports = {
    allRole,    
};
