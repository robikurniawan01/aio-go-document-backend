const mstDepartementModel = require('../../../model/plant_models/master_departement_model');

const searchDepartement = async (req, res) => {
  try {
    const { searchTerm } = req.body; // Ambil parameter pencarian dari body request
    let getDepartement;
    getDepartement = await mstDepartementModel.getMstDepartementByName(searchTerm);

    res.json(getDepartement);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
const allDepartement = async (req, res) => {
    try {      
      getDepartement = await mstDepartementModel.getAllDepartement();
  
      res.json(getDepartement);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };
module.exports = {
    searchDepartement,
    allDepartement
};
