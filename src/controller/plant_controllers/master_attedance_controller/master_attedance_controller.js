const mstAttedanceModel = require('../../../model/plant_models/master_attedance_model');
const getAllAttendance = async (req, res) => {
    try {
      const { meetingId } = req.body; // Ambil parameter pencarian dari body request
      let getDepartement = await mstAttedanceModel.getMstAttedance(meetingId);  
      res.json(getDepartement);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };
  const upsetAttendance = async (req, res) => {
    try {
      const { meetingId, data } = req.body;
  
      // Call the model function to upsert data.
      await mstAttedanceModel.upsertMstAttedance(meetingId, data);
  
      const updatedData = await mstAttedanceModel.getMstAttedance(meetingId);
  
      res.json(updatedData);
    } catch (error) {
      res.status(500).json({ error: error });
    }
  };
  module.exports = {
    getAllAttendance,   
    upsetAttendance 
};
