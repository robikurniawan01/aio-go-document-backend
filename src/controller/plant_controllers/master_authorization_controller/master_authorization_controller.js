const mstAuthModel = require('../../../model/plant_models/master_authorization_model');

const allAuth = async (req, res) => {
  try {      
   
   const getAuth = await mstAuthModel.getAllAuth();
    res.json(getAuth);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
const getAllAuthWithDepartement = async (req, res) => {
  try {
    const { page, itemsPerPage } = req.body; // Ambil parameter paginasi dari body request
    const getAuth = await mstAuthModel.getAllAuthJoin(page, itemsPerPage);
    res.json(getAuth);
  } catch (error) {    
    res.status(500).json({ error: error });
  }
};

const deleteAuthById = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari body request
    if (!id) {
      return res.status(400).json({ error: 'ID is required' });
    }

    // Panggil fungsi delete di model
    const deletedRows = await mstAuthModel.deleteAuthById(id);

    if (deletedRows === 1) {
      // Data berhasil dihapus
      res.json({ message: 'Authorization data has been deleted.' });
    } else {
      // Data tidak ditemukan atau gagal dihapus
      res.status(404).json({ error: 'Authorization data not found.' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
const createAuth = async (req, res) => {
  try {
    const authData = req.body; // Ambil data Authorization dari body request

    // Panggil fungsi create di model
    const insertedIds = await mstAuthModel.createAuth(authData);

    if (insertedIds.length > 0) {
      // Data berhasil dibuat, Anda dapat mengembalikan data yang baru dibuat jika diperlukan.
      // Jika tidak perlu mengembalikan data, Anda dapat mengirimkan pesan sukses.
      res.status(201).json({ status: 'Authorization data has been created.', createdIds: insertedIds });
    } else {
      // Gagal membuat data
      res.status(500).json({ error: 'Failed to create Authorization data.' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Internal error' });
  }
};


const updateAuth = async (req, res) => {
  try {
    const { id } = req.params; // Ambil ID dari URL parameter
    const updatedAuthData = req.body; // Ambil data Authorization yang diperbarui dari body request

    // Panggil fungsi update di model
    const updatedRows = await mstAuthModel.updateAuth(id, updatedAuthData);

    if (updatedRows === 1) {
      // Data berhasil diperbarui
      res.json({ status: 'Authorization data has been updated.' });
    } else {
      // Data tidak ditemukan atau gagal diperbarui
      res.status(404).json({ error: 'Authorization data not found or failed to update.' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
module.exports = {
  allAuth,
  getAllAuthWithDepartement,
  deleteAuthById,
  createAuth,
  updateAuth
};
