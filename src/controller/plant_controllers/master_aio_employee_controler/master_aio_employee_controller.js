const mstEmployeeModel = require('../../../model/plant_models/master_aio_employee_model');

const searchEmployee = async (req, res) => {
  try {
    const { searchTerm, searchBy } = req.body; // Ambil parameter pencarian dari body request
    let getEmployee;

    if (searchBy === 'employee_code') {
      getEmployee = await mstEmployeeModel.getMstEmploymentByCode(searchTerm);
    } else if (searchBy === 'employee_name') {
      getEmployee = await mstEmployeeModel.getMstEmploymentByName(searchTerm);
    }

    res.json(getEmployee);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
const allEmployee = async (req,res) => {
  try {
    let getAllEmployee = await mstEmployeeModel.getAllMstEmployment();
    res.json(getAllEmployee);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
}
module.exports = {
  searchEmployee,
  allEmployee
};
