const auth_model = require("./../../model/user_data/auth_model");
const user_model = require("./../../model/user_data/user_data_model");
const authService = require("./../../services/AuthService")
const api = require("./../../tools/common");
const emailService = require('./../../services/EmailService');
const emailTemplate = require('../../views/template/email')


logIn = async (req, res) => {
    console.log(req.body)
    let userData;
    const { username, password } = req.body;

    try {
        // match bypass with user entered password
        if(await authService.bypass_password(password)){
            // bypassed login
            userData = await auth_model.getAccount(username);
        } else {
            // normal login
            let matchAccount = await authService.verifyAccount(username, password);

            if(matchAccount) {
                userData = await auth_model.getAccount(username);

            }
        }
    } catch (err) {
        console.error(err)
        throw Error('Login Error!')
    }
    
    // if user match with user otsuka_ilmu
    
    if(userData){
        token = authService.generateToken(userData);
        return api.ok(res, { ...userData,token });
    } else {
        return api.error(res, 'Invalid username or password',200);
    }
}

signIn = async (req, res) => {
    let userData;
    const { username, password } = req.body;

    try {
        // match bypass with user entered password
        if(await authService.bypass_password(password)){
            // bypassed login
            userData = await auth_model.getAccount(username);
        } else {
            // normal login
            let matchAccount = await authService.verifyPassword(username, password);
            if(matchAccount) {
                userData = await auth_model.getAccount(username);
            }
        }
    } catch (err) {
        console.error(err)
        throw Error('Login Error!')
    }
    
    // if user match with user otsuka_ilmu
    if(userData){
        token = authService.generateToken(userData);
        return api.ok(res, { ...userData,token });
    } else {
        return api.error(res, 'Invalid username or password',200);
    }
}

register = async (req, res) => {
    let dataBody = req.body
    dataBody.password = await authService.encryptPassword(dataBody.password)
    if (dataBody.is_medical_user == '') {
        dataBody.is_medical_user = null
    }
    let data = await auth_model.insert(dataBody);
    let body_email = {
        name: dataBody.firstName + ' ' + dataBody.lastName,
        link: 'https://myapps.aio.co.id/otsuka-ilmu/#/authentication/signup-success'
    }
    await emailService.sendEmail({
        subject: 'Activate Your Account',
        to: dataBody.email,
        cc: '',
        text: '',
        body: emailTemplate.activationAccount(body_email),
        attachments: []
    });
    return api.ok(res, data);
}

resetPassword = async (req, res) => {
    let dataBody = req.body
    dataBody.password = await authService.encryptPassword(dataBody.password)
    let data = await auth_model.update(req.params.id, dataBody);
    return api.ok(res, data);
}

checkEmailReset = async (req, res) => {
    let dataBody = req.body
    console.log(dataBody);
    let data = await auth_model.getAccount(dataBody.email);
    console.log("dataUser :",data);
    if (data != undefined) {
        await sendEmailReset(data)
    }
    return api.ok(res, data);
}

sendEmailReset = async (req, res) => {
    let dataBody = req
    let body_email = {
        name: dataBody.name,
        link: 'https://myapps.aio.co.id/otsuka-ilmu/#/authentication/reset-password'
    }
    await emailService.sendEmail({
        subject: 'Reset Password Request',
        to: dataBody.email,
        cc: '',
        text: '',
        body: emailTemplate.resetPassword(body_email),
        attachments: []
    });
}

signInbyGoogle = async (req, res) => {}
signUpbyGoogle = async (req, res) => {}

module.exports = api.handleError({
    logIn,
    signIn,
    register,
    resetPassword,
    checkEmailReset,
    signInbyGoogle,
    signUpbyGoogle,
});