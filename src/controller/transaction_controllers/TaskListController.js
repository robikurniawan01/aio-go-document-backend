const database = require('../../database/otsuka_ilmu.dbconfig');
const tasklist = require("../../model/transaction_data/tasklist_model");
const api = require("../../tools/common");

exports.getAll = async (req, res) => {
    try {
        const data = await database.raw(
            `
            SELECT
            a.*,
            b.title as subject,
            b.is_private as publish,
            b.therapeuticals as area,
            b.writer,
            a.created_at as date
            FROM
            tr_tasklist a
            JOIN v_article b ON a.param_id = b.id
            WHERE a.status NOT IN ('approved')
            `
            )

        api.ok(res, data[0]);
    } catch (error) {
        console.log(error);
        api.error(res, { message: error.message });
    }
} 


exports.getWhere = async (req, res) => {
    try {
        const data = await database.raw(
            `
            SELECT
            a.*,
            b.title as subject,
            b.is_private as publish,
            b.therapeuticals as area,
            b.writer,
            a.created_at as date
            FROM
            tr_tasklist a
            JOIN v_article b ON a.param_id = b.id
            WHERE a.status NOT IN ('approved')
            AND (a.user_id = '${req.body.user_id}' OR a.group_id = '${req.body.role_id}')
            `
            )

        api.ok(res, data[0]);
    } catch (error) {
        console.log(error);
        api.error(res, { message: error.message });
    }
} 
