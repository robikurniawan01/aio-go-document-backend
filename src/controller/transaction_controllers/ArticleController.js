const article = require("../../model/article/article");
const article_comment = require("../../model/article/article_comment");
const AuthService = require('../../services/AuthService');
const api = require("../../tools/common");

exports.getAllArticle = async (req, res) => {
    console.log("article",req.query);
    try {
        const { page, size } = req.query;
        const data = await article.findAll(page, size);

        api.ok(res, data);
    } catch (error) {
        console.log(error);
        api.error(res, { message: error.message });
    }
} 

exports.getArticle = async (req, res) => {
    try {
        const conditions = {
            slug_url: req.params.slug
            // put other conditions here
        }
        const data = await article.findOne(conditions);

        api.ok(res, data);
    } catch (error) {
        console.log(error);
        api.error(res, { message: error.message });
    }
}

exports.postComment = async (req, res) => {
    try {
        let { article_id, parent_id, comment, name, email } = req.body;
        let body = {
            article_id: article_id,
            // parent_id: parent_id ,
            comment: comment,
            name: name,
            email: email,
            created_at: new Date(),
            updated_at: new Date(),
            // created_by: name + ';' + email
        }

        const data = await article_comment.create(body);
        api.ok(res, data);
    } catch (error) {
        console.log(error);
        api.error(res, { message: error.message });
    }
}

exports.getComments = async (req, res) => {
    try {
        const { page, size, id } = req.query;
        let conditions = {}

        if (id) {
            conditions = {
                article_id: id
            }
        }

        const data = await article_comment.findAll(page, size, conditions);

        api.ok(res, data);
    } catch (error) {
        console.log(error);
        api.error(res, { message: error.message });
    }
}