// Model (tr_article_model.js)
const db = require('../../database/otsuka_ilmu.dbconfig');

const getAllTArticle = () => {
  return db('tr_article').select('*').orderBy('id','desc');
};

const getTArticlesByRole = (role) => {
  if (!role) {
    return getAllTArticle();
  }

  return db('tr_article')
    .select('*')
    .where('is_private', role).orderBy('id','desc');
};

const getAllTArticleWithPagination = (offset, limit) => {
  return db('tr_article')
    .select('*')
    .orderBy('id','desc')
    .offset(offset)
    .limit(limit);
};

const getTArticlesByRoleWithPagination = (role, offset, limit) => {
  if (!role) {
    return getAllTArticleWithPagination(offset, limit);
  }

  return db('tr_article')
    .select('*')
    .where('is_private', role)
    .orderBy('id','desc')
    .offset(offset)
    .limit(limit);
};
// micro_site/tr_article_model.js

// Mengambil total semua rekaman
const getTotalRecords = async () => {
  return db('tr_article').count('id as total');  
};

// Mengambil total semua rekaman berdasarkan peran
const getTotalRecordsByRole = async (role) => {
  return db('tr_article').where('is_private', role).count('id as total');
};


module.exports = {
  getAllTArticle,
  getTArticlesByRole,
  getAllTArticleWithPagination,
  getTArticlesByRoleWithPagination,
  getTotalRecords,
  getTotalRecordsByRole,
};
