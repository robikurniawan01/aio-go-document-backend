const db = require('../../database/otsuka_ilmu.dbconfig');

// Mendapatkan semua data tim
const getAllFaq = () => {
  return db('mst_faq').select('*');
};


module.exports = {
    getAllFaq,
};
