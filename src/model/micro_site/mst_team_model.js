const db = require('../../database/otsuka_ilmu.dbconfig');

// Mendapatkan semua data tim
const getAllTeams = () => {
  return db('mst_team').select('*');
};


module.exports = {
  getAllTeams,
};
