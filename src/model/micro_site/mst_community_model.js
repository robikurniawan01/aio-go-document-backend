const db = require('../../database/otsuka_ilmu.dbconfig');

// Mendapatkan semua data tim
const getAllCommunity = () => {
  return db('mst_community').select('*');
};


module.exports = {
    getAllCommunity,
};
