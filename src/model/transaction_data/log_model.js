const database = require('../../database/otsuka_ilmu.dbconfig');


// INSERT DATA

/**
 * Insert single row
 * @param {String} table - table name
 * @param {object} data - json/object to be added to table
 * @return {number} The id of row data that was succesfully added
 */


insert = async (request, type, message) => {
    const {
        method,
        url,
        headers,
        body,
        statusCode,
        responseHeaders,
        responseBody,
        ip,
        user_id,
        username,
        session_id,
        additional_info,
    } = request;

    await database('tr_log').insert({
        type,
        message,
        request_method: method,
        request_url: url,
        request_headers: JSON.stringify(headers),
        request_body: JSON.stringify(body),
        response_status_code: statusCode,
        response_headers: JSON.stringify(responseHeaders),
        response_body: JSON.stringify(responseBody),
        client_ip: ip,
        user_agent: request.headers['user-agent'],
        user_id,
        username,
        session_id,
        additional_info: JSON.stringify(additional_info),
    });
}

module.exports = { insert };