const database = require('../../database/aio_go_Document.dbconfig ');

// GET DATA

/**
 * Get all specific master data
 * @param {String} table - table name
 * @returns {Array<Object>} Array of table data
 */
getAll = async (table) => await database.select('*').from(table).where('status', '!=', 'deleted');

/**
 * Get single specific master data by id
 * @param {String} table - table name
 * @param {number} id - id of the table
 * @return {object} The table object
 */
getById = async (table, id) => await database.select('*').from(table).where('id', id);

/**
 * Get all data by column value
 * @param {string} table - table name
 * @param {array<{field, operator, value}>} consditions - filter conditions
 * @returns {array<Object>} Array of table data
 */
getWhere = async (table, conditions = [], page = 1, size = 1000) => {
    let query = database.select('*').from(table).where('status', '!=', 'deleted');
    conditions.forEach(condition => {
        query = query.where(condition.field, condition.operator, condition.value);
    });
    return await query.limit(size).offset((page - 1) * size);
}

// INSERT DATA

/**
 * Insert single row
 * @param {String} table - table name
 * @param {object} data - json/object to be added to table
 * @return {number} The id of row data that was succesfully added
 */
insert = async (table, data) => await database(table).insert(data);

// UPDATE DATA

/**
 * Update table Data
 * @param {String} table - table name
 * @param {number} id - id of table need to be edited
 * @param {object} data - json/object to be updated to table
 * @return {number} The id of updated row data
 */
update = async (table, id, data) => await database(table).where('id', id).update(data);

// DELETE DATA

/**
 * Update table Data
 * @param {String} table - table name
 * @param {number} id - id of table need to be deleted
 * @return {number} The id of deleted row data
 */
deleteData = async (table, id) => await database(table).where('id', id).del();

/**
 * Update table Data
 * @param {String} table - table name
 * @param {number} id - id of table need to be deleted
 * @return {number} The id of deleted row data
 */
deleteDataWhere = async (table, column, value) => await database(table).where(column, value).del();

module.exports = {
    getAll,
    getById,
    getWhere,
    insert,
    update,
    deleteData,
    deleteDataWhere
};