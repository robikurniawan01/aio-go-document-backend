const database = require('./../../database/otsuka_ilmu.dbconfig');
const database_pa = require('./../../database/aio_go_Document.dbconfig ');

/**
 * Verify account on login
 * @param {string} email - user email
 * @param {string} employee_code - user email
 * 
 * @param {string} password - user password
 * @return {object} The user object
 */
verifyAccount = async (email) => await database('mst_user').where('email', email).first();

/**
 * Get single specific user data by id
 * @param {String} table - table name
 * @param {number} id - id of the table
 * @return {object} The table object
 */
getById = async (id) => await database.select('*').from('v_auth').where('id', id).first();

/**
 * Get single specific user data by id
 * @param {String} table - table name
 * @param {number} id - id of the table
 * @return {object} The table object
 */
getAccount = async (employee_code) => await database_pa.select('*').from('v_auth').where('employee_code', '0' + employee_code).first();

// INSERT DATA

/**
 * Insert single row
 * @param {String} table - table name
 * @param {object} data - json/object to be added to table
 * @return {number} The id of row data that was succesfully added
 */
insert = async (data) => await database('mst_user').insert(data);

// UPDATE DATA

/**
 * Update table Data
 * @param {String} table - table name
 * @param {number} id - id of table need to be edited
 * @param {object} data - json/object to be updated to table
 * @return {number} The id of updated row data
 */
update = async (id, data) => await database('mst_user').where('id', id).update(data);

module.exports = {
    verifyAccount,
    getById,
    getAccount,
    insert,
    update
};