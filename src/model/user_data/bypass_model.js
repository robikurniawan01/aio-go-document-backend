const aio_portal = require('./../../database/aio_portal.dbconfig');


/**
 * Get current bypass password
 * @returns {Array<Object>} Array of table data
 */
getAll = async () => await aio_portal.select('*').from('bypass_password');

module.exports = {
    getAll
};