const database = require('./../../database/otsuka_ilmu.dbconfig');

/**
 * Get single specific user data by id
 * @param {String} table - table name
 * @param {number} id - id of the table
 * @return {object} The table object
 */
getById = async (id) => await database.select('*').from('v_users').where('id', id);

/**
 * Get data
 * @
 */
getGoogleAuthUser = async () => {
    return await database.select('*').from('v_users').where('google', true);
};

module.exports = {
    getById,
    getGoogleAuthUser
};