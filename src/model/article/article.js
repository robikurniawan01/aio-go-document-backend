const database = require('./../../database/otsuka_ilmu.dbconfig');
const TABLE_NAME = "tr_article";

const Models = {
    findAll: async (pageNumber = 1, itemsPerPage = 100, conditions = {}) => {
        const offset = (pageNumber - 1) * itemsPerPage;

        try {
            const result = await database
                .select('*')
                .from(TABLE_NAME)
                .where(conditions)
                .limit(itemsPerPage)
                .offset(offset);
                

            return result;
        } catch (error) {
            // Handle database errors here
            throw new Error(`Error fetching data: ${error.message}`);
        }
    },
  
    findOne: async (conditions) => {
        return database.select('*').from(TABLE_NAME).where(conditions).first();
      },
  
    create: async (data) => {
      return database(TABLE_NAME).insert(data).returning('*');
    },
  
    update: async (id, data) => {
      return database(TABLE_NAME).where('id', id).update(data).returning('*');
    },
  
    delete: async (id) => {
      return database(TABLE_NAME).where('id', id).del();
    },

    countPostStatus: async() => {
      return database(TABLE_NAME).select('post_status').count('id as count').groupBy('post_status');
    },

    countWhere: async(conditions=[]) => {
      let query = database(TABLE_NAME).select('id').count('id as count').where('status', '!=', 'deleted');
      conditions.forEach(condition => {
          query = query.where(condition.field, condition.operator, condition.value);
      });
      return await query;
    }
  
    // Add more generic model functions as needed
  };

module.exports = Models;