const database = require('../../database/otsuka_ilmu.dbconfig');
const TABLE_NAME = "tr_article_comment";

const Models = {
    findAll: async (pageNumber = 1, itemsPerPage = 100, conditions = {}) => {
        const offset = (pageNumber - 1) * itemsPerPage;

        async function getComments(parentId = null) {
          conditions.parent_id = parentId;
          const comments = await database
            .select('*')
            .from(TABLE_NAME)
            .where(conditions)
            .limit(itemsPerPage)
            .offset(offset);
      
          const nestedComments = [];
      
          for (const comment of comments) {
              const replies = await getComments(comment.id); // Recurse to get replies for this comment
              nestedComments.push({ ...comment, replies });
          }
      
          return nestedComments;
      }
      
      // Call the recursive function to get the nested comments
      const nestedComments = await getComments();

      return nestedComments;
    },

    findOne: async (conditions) => {
        try {
            const result = await database
                .select('*')
                .from(TABLE_NAME)
                .where(conditions)
                .first();

            return result;
        } catch (error) {
            // Handle database errors here
            throw new Error(`Error fetching data: ${error.message}`);
        }
    },

    create: async (data) => {
        try {
            const result = await database(TABLE_NAME).insert(data).returning('*');
            return result;
        } catch (error) {
            // Handle database errors here
            throw new Error(`Error inserting data: ${error.message}`);
        }
    },

    update: async (id, data) => {
        try {
            const result = await database(TABLE_NAME).where('id', id).update(data).returning('*');
            return result;
        } catch (error) {
            // Handle database errors here
            throw new Error(`Error updating data: ${error.message}`);
        }
    },

    delete: async (id) => {
        try {
            const result = await database(TABLE_NAME).where('id', id).del();
            return result;
        } catch (error) {
            // Handle database errors here
            throw new Error(`Error deleting data: ${error.message}`);
        }
    },
};

module.exports = Models;