const db = require('../../database/aio_plant_Action.dbconfig');

const getMstAttedance = (meetingId) => {
  return db('tr_monthly_attendance').where('id_meeting', meetingId).select('*');
};

const upsertMstAttedance = async (meetingId, data) => {
  const existingData = await db('tr_monthly_attendance').where('id_meeting', meetingId);

  for (const item of data) {
    const existingRecord = existingData.find((record) => record.employee_code === item.employee_code);

    if (existingRecord) {
      if (!item.checked) {
        // Delete the record if 'checked' is false.
        await db('tr_monthly_attendance').where('id', existingRecord.id).del();
      }
    } else if(item.checked) {
      // Insert a new record if it doesn't exist.
      await db('tr_monthly_attendance').insert({
        employee_code: item.employee_code,        
        id_meeting: meetingId,
        status : 'Present'
        
      });
    }
  }
};


module.exports = {
  getMstAttedance,
  upsertMstAttedance,
};
