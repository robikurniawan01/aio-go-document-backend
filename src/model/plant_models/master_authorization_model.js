const db = require('../../database/aio_plant_Action.dbconfig');

const getAllAuth = () => {
  return db('mst_authorization').join('mst_department', 'mst_authorization.department_id', 'mst_department.id')
  .select(
    'mst_authorization.id',
    'mst_authorization.employee_code',
    'mst_authorization.employee_name',
    'mst_department.name as name_departement',
    'mst_department.id as department_id',
    'mst_department.code as code_departement',
    'mst_authorization.role',
    db.raw("DATE_FORMAT(mst_authorization.created_at, '%Y-%m-%d') as created_at"), // Mengambil hanya tanggal
    'mst_authorization.status');
}

const getAllAuthJoin = async (page, itemsPerPage) => {
  const results = await db('mst_authorization')
    .join('mst_department', 'mst_authorization.department_id', 'mst_department.id')
    .select(
      'mst_authorization.id',
      'mst_authorization.employee_code',
      'mst_authorization.employee_name',
      'mst_department.name as name_departement',
      'mst_department.id as department_id',
      'mst_department.code as code_departement',
      'mst_authorization.role',
      db.raw("DATE_FORMAT(mst_authorization.created_at, '%Y-%m-%d') as created_at"), // Mengambil hanya tanggal
      'mst_authorization.status'
    );

  return results;
};

const deleteAuthById = (id) => {
  return db('mst_authorization').where({ id: id }).del();
};
const createAuth = (authData) => {
  return db('mst_authorization').insert(authData);
};

const updateAuth = (id, updatedAuthData) => {
  return db('mst_authorization').where({ id: id }).update(updatedAuthData);
};
module.exports = {
    getAllAuth,
    getAllAuthJoin,
    deleteAuthById,
    createAuth,
    updateAuth
};
