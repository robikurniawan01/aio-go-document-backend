const db = require('../../database/aio_plant_Action.dbconfig');

const getAllDepartement = () => {
  return db('mst_department').select('*');
};

const getMstDepartementByName = (departementName) => {
  return db('mst_department').where('name', 'like', `%${departementName}%`).select('*');
};
const getDepartementById = (id) => {
  return db('mst_department').where('id', id).first();
};

module.exports = {
    getAllDepartement,
    getMstDepartementByName,
    getDepartementById,
};
