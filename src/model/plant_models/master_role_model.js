const db = require('../../database/aio_plant_Action.dbconfig');

const getAllRole = () => {
  return db('mst_role').select('*');
};

module.exports = {
    getAllRole,
};
