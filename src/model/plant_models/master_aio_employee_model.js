const db = require('../../database/aio_employee.dbconfig');

const getAllMstEmployment = () => {
  return db('mst_employment').select('*').where(function() {
    this.whereNot('job_grade_code', 'like', 'S1%')
      .andWhereNot('job_grade_code', 'like', 'S2%')
      .andWhereNot('job_grade_code', 'like', 'S3%');
  });
};

const getMstEmploymentByCode = (employeeCode) => {
  return db('mst_employment').where('employee_code', 'like', `%${employeeCode}%`).select('*');
};

const getMstEmploymentByName = (employeeName) => {
  return db('mst_employment')
    .where('employee_name', 'like', `%${employeeName}%`)
    .where(function() {
      this.whereNot('job_grade_code', 'like', 'S1%')
        .andWhereNot('job_grade_code', 'like', 'S2%')
        .andWhereNot('job_grade_code', 'like', 'S3%');
    })
    .select('*');
};



module.exports = {
  getAllMstEmployment,
  getMstEmploymentByCode,
  getMstEmploymentByName,
};
