var nodemailer = require( 'nodemailer')
// var moment = require('moment')

async function sendEmail(data){
	return new Promise((resolve, reject)=> {
		var transport = nodemailer.createTransport({
			host: process.env.MAIL_HOST,
			port: process.env.MAIL_PORT,
			secure: false,
			auth: {
				user: process.env.MAIL_USER,
				pass: process.env.MAIL_PASS
			},
			tls: {rejectUnauthorized: false},
			debug: true
		});

		var message = {
			subject : data.subject,
			from    : '"OTSUKA ILMU" <appsskb@aio.co.id>',
			to      : process.env.PROD == "FALSE"? process.env.EMAIL_RECIPIENTS : data.to,
			cc 		: data.cc,
			text    : data.text,
			html    : process.env.PROD == "FALSE"? `=======<br>DEV MODE<br>=======<br>Actual Recipient: ${data.to}<br><br>${data.body}` : data.body,
			attachments    : data.attachments
		};
	
		transport.sendMail(message,function (err) {
			if(err){
				console.log(err);
				reject(err)
			}else{
				resolve(true)
			}
		});
	})
}

module.exports = {
	sendEmail
};