const fs = require('fs');
const EmailService = require('./EmailService');
const WhatsappService = require('./WhatsappService');
const user_model = require('./../model/user_data/user_data_model');



/**
 * Generate config | EDITABLE
 * 
 * @param {*} type 
 * @returns 
 */
const generateConfig = async (type) => {
    let files = {};
    let target_user = [];
    let recipients = {email:[], whatsapp:[]};

    //edit if have different type of notification
    if(type == 'StartInvestigation') {
        target_user = await user_model.getWhere('mst_user', 'role_id', 3);  
        files.email = "bodyEmailNotifInvestigation.html";
        files.whatsapp = "bodyWhatsappNotifInvestigation.txt";
    } else if(type == 'UpdateInvestigation'){
        target_user = await user_model.getWhere('mst_user', 'role_id', 2);
        files.email = "bodyEmailNotifUpdateInvestigation.html";
        files.whatsapp = "bodyWhatsappNotifUpdateInvestigation.txt"; 
    } else if(type == 'StartComplaint'){
        target_user = await user_model.getWhere('mst_user', 'role_id', 3);  
        files.email = "bodyEmailNotifComplaint.html";
        files.whatsapp = "bodyWhatsappNotifComplaint.txt";
    } else if(type == 'UpdateComplaint'){
        target_user = await user_model.getWhere('mst_user', 'role_id', 3);  
        files.email = "bodyEmailNotifUpdateComplaint.html";
        files.whatsapp = "bodyWhatsappNotifUpdateComplaint.txt";
    } else {
        throw Error('Notification type not found!');
    }

    for (user of target_user){
        let user_data = await user_model.getAccount(user.id);
        recipients.email.push(user_data.email);
        recipients.whatsapp.push(user_data.phone_number);
    }
    return { recipients, files }
}




/**
 * Funtion fill parameters for body text
 * 
 * @param {string} template 
 * @param {object} param 
 * @returns String
 */
const fillParameters = (template, param) => {
    return template.replace(/\{\{(\w+)\}\}/g, (match, key) => param[key] || match);
}


/**
 * SEND NOTIFICATION FUNCTION | CAREFUL TO EDIT |
 * @param {string} type 
 * @param {object} param 
 * @param {boolean} sendWhatsapp | optional
 * @param {boolean} sendEmail  | optional
 * @param {string[]}  recipients  | optional
 * @param {string} files | optional
 * @param {string} title | optional
 * 
 * @return {Boolean}
 */
sendNotification = async (type, param, configParam={})  => {
    //check param
    const sendWhatsapp = configParam.sendWhatsapp? configParam.sendWhatsapp : true
    const sendEmail = configParam.sendEmail? configParam.sendEmail : true
    let recipients = configParam.recipients? configParam.recipients : []
    let files = configParam.files? configParam.files : {}
    let title = configParam.title? configParam.title : ""

    //declare folder path
    const emailFolderPath = './src/views/email/';
    const whatsappFolderPath = './src/views/whatsapp/';
    
    //declare body email
    let bodyEmail = "";
    let bodyWhatsapp = "";

    //config variable
    let config = {};

    //fill recipient using defaulr setting if null
    if(param && recipients && files && title){
        config = {
            recipients: recipients,
            files: files,
            title: title,
        }
    } else {
        config = await generateConfig(type);
    }

    //set notification body file path
    let emailFilePath = `${emailFolderPath}${config.files.email}`;
    let whatsappFilePath = `${whatsappFolderPath}${config.files.whatsapp}`;

    try{
        //read body file as text
        bodyEmail = fs.readFileSync(emailFilePath, 'utf8');
        bodyWhatsapp = fs.readFileSync(whatsappFilePath, 'utf8');
    } catch(err){
        throw Error(err);
    }

    // set variable inside test body
    bodyEmail = fillParameters(bodyEmail, param);
    bodyWhatsapp = fillParameters(bodyWhatsapp, param);

    if(Boolean(sendWhatsapp)){
        await WhatsappService.sendWhatsapp({
            to: config.recipients.whatsapp,
            body: bodyWhatsapp,
        });
    }
    if(Boolean(sendEmail)){
        await EmailService.sendEmail({
            subject: config.title,
            to: config.recipients.email,
            cc: "",
            text: "Please enable HTML!",
            body: bodyEmail,
            attachments:[]
        });
    }
    return true;
}

module.exports = {
    sendNotification
}