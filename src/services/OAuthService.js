const https = require('https');
const authController = require("../controller/auth_controller/AuthController");
const authService = require("./AuthService")
const auth_model = require("./../model/user_data/auth_model")
const axios = require('axios').create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
  }),
});
const tokenEndpoint = 'https://oauth2.googleapis.com/token';
const userInfoUrl = 'https://www.googleapis.com/oauth2/v1/userinfo';

const getOauth = async (data) => {
    return new Promise((resolve, reject) => {
        axios.post(tokenEndpoint, data)
        .then(async (response) => {
          const accessToken = response.data.access_token;
          resolve(await userInfo(accessToken))
        })
        .catch((error) => {
          reject('error', error)
        });

    }) 
  }
  
  const userInfo = async (accessToken) => {
    return new Promise((resolve, reject) => {
        axios.get(userInfoUrl, {
            headers: {
              'Authorization': `Bearer ${accessToken}`,
            },
          })
          .then(async (response) => {
            let req = response.data;
            const [firstName, lastName] = req.name.split(' ');
            let registerData = {
              first_name: firstName,
              last_name: lastName,
              email: req.email,
              image: req.picture,
              status: 'active',
              is_google_auth: 1
            }
            await auth_model.insert(registerData);
            let userData = {
              id: req.id,
              name: req.name,
              image: req.picture,
              email: req.email,
              is_google_auth: 1,
              role_id: null,
              phone_number: null,
              role_name: null,
              role_detail: null,
              permissions: null
          }
          token = authService.generateToken(userData);
          resolve({ ...userData, token });
          })
          .catch((error) => {
            reject('error', error)
          });
    })
  }
  
  module.exports = { getOauth };