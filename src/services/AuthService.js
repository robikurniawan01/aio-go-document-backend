const secretKey = process.env.SECRET_KEY;
const aio_portal = require("./../model/user_data/bypass_model");
const auth_model = require("./../model/user_data/auth_model");
const api = require('./../tools/common')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const sequelizeQuery = require('./../database/sequelizeQuery');
var md5 = require('md5');

/**
 * Login Service
 * @param {string} password 
 * @returns  {object|boolean}
 */
const bypass_password = async (password) => {
  const data = await aio_portal.getAll();
  return data[0].password == password;
}

const encryptPassword = async (password) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err) {
        reject(err);
      } else {
        resolve(hash);
      }
    });
  });
};

const verifyPassword = async (username, password) => {
  const auth_data = await auth_model.verifyAccount(username);
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, auth_data.password, (err, result) => {
      if (err) {
        reject(err);
      } else if (result) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  })
};

const verifyAccount = async (username, password) => {
  let replacements = { username: username }
  replacements.password = md5(password);

  let query = `select TOP 1 lg_nik, lg_name, lg_location, sectionParent, n_photo from PHP_ms_login where lg_nik = :username COLLATE SQL_Latin1_General_CP1_CS_AS and lg_password= :password COLLATE SQL_Latin1_General_CP1_CS_AS and lg_aktif = '1'`
  let hasil = await sequelizeQuery.sequelizeEmployee.query(query,
    {
      type: sequelizeQuery.sequelizeEmployee.QueryTypes.SELECT,
      replacements: replacements

    }).then(function (data) {
      if (data.length > 0) {
        return true
      } else {
        return false
      }
    
  });
  return  hasil
}

      
      /**
       * 
       * Token Generation
       * 
       * @param {object} user - user information as object
       * @returns {object} token
       */
      const generateToken = (user) => {
        return token = jwt.sign({ ...user }, secretKey, {
          expiresIn: '24h'
        });
      }

      /**
       * Function token checking
       * 
       * @param {object} req - object of resquest 
       * @param {object} res - object of response 
       * @param {function} permission - array of permission 
       * @returns {function} next|res
       */
      const authenticateToken = (req, res, next) => {
        let token = req.headers.authorization;

        if (!token) {
          return res.status(401).json({ message: 'Unathorized Access' });
        }
        token = token.replace('Bearer ', '');
        jwt.verify(token, secretKey, (err, decoded) => {
          if (err) {
            return res.status(403).json({ message: 'Invalid token', ...err });
          }
          req.user = decoded; // Attach the decoded user information to the request object
          next();
        });
      };

      const authenticateCSRFToken = (req, res, next) => {
        if (req.method === 'POST' || req.method === 'PUT') {
          console.log('body', req.body._csrf)
          if (req.csrfToken() !== req.body._csrf) {
            return res.status(403).send('Invalid CSRF token.');
          }
        }
        next();
      };

      /**
       * Function access checking
       * 
       * @param {string[]} permission - array of permission 
       * @returns {function}
       */
      const hasAccess = (permissions) => {
        return (req, res, next) => {
          console.log('user', req)
          try {
            permissions.forEach(permission => {
              if (!req.user.permissions.includes(permission)) {
                return res.status(401).json({ message: 'Access Unathorized!' });
              }
            });
            next();
          } catch (err) {
            console.log(err)
            console.warn('Check the routing middleware!')
            return api.error(res, "Incorrect permission status!", 500)
          }
        }
      }

      module.exports = { bypass_password, generateToken, authenticateToken, authenticateCSRFToken, hasAccess, encryptPassword, verifyPassword, verifyAccount };