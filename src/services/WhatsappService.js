var axios = require('axios');


sendWhatsapp = (data) => {
    const recipients = process.env.PROD == 'TRUE'? process.env.WA_RECIPIENTS.split(',') : data.to;
    const message =  process.env.PROD == 'FALSE'? `=======\nDEV MODE\n=======\nActual Recipient:${data.to}\n\n${data.body}` : data.body

    let payload = recipients.map((x, index)=> {
        return {
            attendance_name: `Recipient-${index+1}`,
            attendance_phonenumber: x
        }
    })

    return new Promise(async (resolve, reject) => {
        await axios.post('https://myapps.aio.co.id/notification-center/send-message-bulk', {
            contact: payload,
            message: message
        }, {
            headers: {
                'Content-Type': 'application/json',
                'username': process.env.WA_USERNAME,
                'password': process.env.WA_PASSWORD,
            }
        })
        .then(async function (res) {
            console.log('Message sent succesfully');
        })
        .catch(async function (error) {
            console.log('failed to send message', error);
        });

        resolve(true);
    });
}

module.exports = {
    sendWhatsapp
}
