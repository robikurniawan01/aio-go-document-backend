const multer = require('multer');
const fs = require('fs');
const path = require('path');

// Set up Multer for handling file uploads
const storage = multer.diskStorage({
  destination: 'storage/uploads/',
  filename: (req, file, cb) => {
    const originalname = file.originalname;
    const extname = path.extname(originalname);
    const basename = path.basename(originalname, extname);

    let filename = originalname;
    let i = 1;
    while (fs.existsSync(`storage/uploads/${filename}`)) {
      filename = `${basename} (${i})${extname}`;
      i++;
    }

    cb(null, filename);
  },
});

const fileChecking = function(filename, res) {
    const filePath = path.join(__dirname ,'../..', `/storage/uploads/`, filename); // Construct the file path
    console.log(filePath)
    if (fs.existsSync(filePath)) {
        // Determine the file type based on the extension
        const fileExtension = path.extname(filename).toLowerCase();
        // Set the appropriate Content-Type based on the file extension
        if (fileExtension === '.pdf') {
            res.setHeader('Content-Type', 'application/pdf');
        } else if (['.jpg', '.jpeg', '.png', '.gif'].includes(fileExtension)) {
            res.setHeader('Content-Type', `image/${fileExtension.substring(1)}`);
        } else {
            // Unsupported file type
            return false;
        }
        return filePath;
    } else {
        return false;
    }
}

const upload = multer({ storage: storage });


const download = function(req, res) {
    const filePath = fileChecking(req.params.filename, res);
    if(filePath){
        const contentDisposisiton = req.query.inline == 'true'? 'inline' : 'attachment';
        // Set the headers to indicate inline content and specify the filename
        res.setHeader('Content-Disposition', `${contentDisposisiton}; filename="${req.params.filename}"`);
        // Read the file and stream it back to the client
        const fileStream = fs.createReadStream(filePath);
        console.log(contentDisposisiton);
        fileStream.pipe(res);
    } else {
      res.redirect('/api/not-found');
    }
}

module.exports = { upload, download };
