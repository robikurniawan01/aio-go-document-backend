const log = require('./../model/transaction_data/log_model')

// Service as middleware
logMiddleware = async (req, res, next) => {
    try {
        await log.insert(req, 'LOG');
        return next();
    } catch (err) {
        console.log(err)
        return res.status(500).json({ message: 'Service Unavailable'});
    }
}


// Service as logging function
insertLog = async (req, type="LOG", message=null) => {
    try {
        await log.insert(req, type, message);
        return true;
    } catch (err) {
        console.log(err)
        return false;
    }
}

module.exports = { logMiddleware, insertLog }