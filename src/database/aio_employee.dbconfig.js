const AioEmployeeDb = require('knex')({
    client: 'mysql',
    connection: {
      host : process.env.DB_AIOEMPLOYE_HOST,
      port : process.env.DB_AIOEMPLOYE_PORT,
      user : process.env.DB_AIOEMPLOYE_USER,
      password : process.env.DB_AIOEMPLOYE_PASSWORD,
      database : process.env.DB_AIOEMPLOYE_DATABASE
    },
    useNullAsDefault: false,
    log: {
      warn(message) {
        console.log(message)
      },
      error(message) {
        console.log(message)
      },
      deprecate(message) {
        console.log(message)
      },
      debug(message) {
        console.log(message)
      }
    }
});

AioEmployeeDb.on( 'query', function( queryData ) {
  // console.log('=============== EXECUTED QUERY ===============');
  // console.log( queryData.sql );
});

AioEmployeeDb.client.config.connectionOptions = { multipleStatements: false };
// AioEmployeeDb.on('query', (query) => console.log('Query:', query.sql)); // log every query DEV mode

module.exports = AioEmployeeDb;