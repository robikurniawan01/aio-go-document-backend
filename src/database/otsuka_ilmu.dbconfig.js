const OtsukaIlmuDB = require('knex')({
    client: 'mysql',
    connection: {
      host : process.env.DB_ILMU_HOST,
      port : process.env.DB_ILMU_PORT,
      user : process.env.DB_ILMU_USER,
      password : process.env.DB_ILMU_PASSWORD,
      database : process.env.DB_ILMU_DATABASE
    },
    useNullAsDefault: false,
    log: {
      warn(message) {
        console.log(message)
      },
      error(message) {
        console.log(message)
      },
      deprecate(message) {
        console.log(message)
      },
      debug(message) {
        console.log(message)
      }
    }
});

OtsukaIlmuDB.on( 'query', function( queryData ) {
  // console.log('=============== EXECUTED QUERY ===============');
  // console.log( queryData.sql );
});

OtsukaIlmuDB.client.config.connectionOptions = { multipleStatements: false };
// OtsukaIlmuDB.on('query', (query) => console.log('Query:', query.sql)); // log every query DEV mode

module.exports = OtsukaIlmuDB;