const OtsukaPlantAppDB = require('knex')({
    client: 'mysql',
    connection: {
      host : process.env.DB_PLANTAPP_HOST,
      port : process.env.DB_PLANTAPP_PORT,
      user : process.env.DB_PLANTAPP_USER,
      password : process.env.DB_PLANTAPP_PASSWORD,
      database : process.env.DB_PLANTAPP_DATABASE
    },
    useNullAsDefault: false,
    log: {
      warn(message) {
        console.log(message)
      },
      error(message) {
        console.log(message)
      },
      deprecate(message) {
        console.log(message)
      },
      debug(message) {
        console.log(message)
      }
    }
});

OtsukaPlantAppDB.on( 'query', function( queryData ) {
  // console.log('=============== EXECUTED QUERY ===============');
  // console.log( queryData.sql );
});

OtsukaPlantAppDB.client.config.connectionOptions = { multipleStatements: false };
// OtsukaPlantAppDB.on('query', (query) => console.log('Query:', query.sql)); // log every query DEV mode

module.exports = OtsukaPlantAppDB;