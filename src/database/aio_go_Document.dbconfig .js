const OtsukaGoDocAppDB = require('knex')({
    client: 'mysql',
    connection: {
      host : process.env.DB_GODOCUMENT_HOST,
      port : process.env.DB_GODOCUMENT_PORT,
      user : process.env.DB_GODOCUMENT_USER,
      password : process.env.DB_GODOCUMENT_PASSWORD,
      database : process.env.DB_GODOCUMENT_DATABASE
    },
    useNullAsDefault: false,
    log: {
      warn(message) {
        console.log(message)
      },
      error(message) {
        console.log(message)
      },
      deprecate(message) {
        console.log(message)
      },
      debug(message) {
        console.log(message)
      }
    }
});

OtsukaGoDocAppDB.on( 'query', function( queryData ) {
  console.log('=============== EXECUTED QUERY ===============');
  console.log( queryData.sql );
});

OtsukaGoDocAppDB.client.config.connectionOptions = { multipleStatements: false };
// OtsukaGoDocAppDB.on('query', (query) => console.log('Query:', query.sql)); // log every query DEV mode

module.exports = OtsukaGoDocAppDB;