var Sequelize = require("sequelize");

const sequelizeEmployee = new Sequelize(process.env.DB_EMPLOYEE_DATABASE, process.env.DB_EMPLOYEE_USER, process.env.DB_EMPLOYEE_PASSWORD, {
    host: process.env.DB_EMPLOYEE_HOST,
    dialect: process.env.DB_EMPLOYEE_DIALECT,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    timezone: "+07:00",
    logging: false,
    operatorsAliases: 0,
});

module.exports = {
sequelizeEmployee
};