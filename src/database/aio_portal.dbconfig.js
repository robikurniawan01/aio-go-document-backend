const aio_portal = require('knex')({
    client: 'mysql',
    connection: {
      host : process.env.DB_PORTAL_HOST,
      port : process.env.DB_PORTAL_PORT,
      user : process.env.DB_PORTAL_USER,
      password : process.env.DB_PORTAL_PASSWORD,
      database : process.env.DB_PORTAL_DATABASE
    },
    useNullAsDefault: false,
    log: {
      warn(message) {
        console.log(message)
      },
      error(message) {
        console.log(message)
      },
      deprecate(message) {
        console.log(message)
      },
      debug(message) {
        console.log(message)
      }
    }
});


aio_portal.client.config.connectionOptions = { multipleStatements: false };

module.exports = aio_portal;