const express = require("express");
const mainRoutes = require("./src/routes/routes")
const app = express();
const cors = require('cors');
const cookieParser = require('cookie-parser');
const path = require('path');
const csrf = require('csurf');
// const rateLimitMiddleware = require('./src/tools/rateLimiter')
const bodyParser = require('body-parser');
const oauthService = require('./src/services/OAuthService')


// Parse URL-encoded data (including query parameters)
app.use(bodyParser.urlencoded({ extended: true }));

// application/json & cookie parser
app.use(express.json());
app.use(cookieParser());

// host static folder for uploaded files
app.use('/uploads', express.static(path.join(__dirname, 'storage/uploads')));

// Check the value of the PROD environment variable
// const isProduction = process.env.PROD === 'TRUE' ? true : false;
// const csrfProtection = csrf({
//   cookie: true, // Enable cookies for CSRF tokens
// });
const isProduction = process.env.PROD === 'TRUE' ? true : false;
const csrfProtection = csrf({ cookie: { httpOnly: true, } });

// Conditionally apply CORS middleware if isProduction is true
if (isProduction) {
  const allowedOrigins = ['https://myapps.aio.co.id', 'https://otsuka-ilmu.aio.co.id'];
  const corsOptions = {
    origin: (origin, callback) => {
      if (allowedOrigins.includes(origin)) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
  };
  // Apply CORS, anti-CSRF, rate limiter to API routes only
  app.use('/api', cors(corsOptions));
  app.use('/api', csrfProtection);
  // app.use(rateLimitMiddleware);
} else {
  // app.use('/api', csrfProtection);
  app.use('/api', cors());
}
// application/json & cookie parser
app.use(express.json());
app.use(cookieParser());

// open cors 
app.use(function (req, res, next) {
  const allowedOrigins = ['http://localhost:4200', 'https://myapps.aio.co.id', 'https://otsuka-ilmu.aio.co.id'];
  // Mengizinkan akses hanya dari daftar origin yang telah didefinisikan
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }

  // Request methods yang diizinkan
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers yang diizinkan
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set ke true jika Anda memerlukan website untuk mengirim cookies dalam permintaan (misalnya, dalam kasus penggunaan sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Lanjutkan ke lapisan middleware berikutnya
  next();
});


// parse requests of content-type - application/x-www-form-urlencoded for files
app.use(express.urlencoded({ extended: true, limit: '10mb' }));

// use all routes defined in routes file
app.use('/api', mainRoutes);

// get CSRF
app.get('/init-post-request', csrfProtection, (req, res) => {
  // The csrfToken() function is available on the request object
  const csrfToken = req.csrfToken();
  res.status(200).json({ csrfToken });
});

// redirect any other routes
app.use((req, res) => res.redirect('/api/not-found'));

// set port, listen for requests
const PORT = process.env.PORT || 8800;
app.listen(PORT, () => {
  console.log(`Go Document API Service is running on port ${PORT}. ${process.env.PROD ? "" : "<Development Mode>"}`);
});
